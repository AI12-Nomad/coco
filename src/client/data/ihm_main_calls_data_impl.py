from __future__ import annotations

from typing import List
import uuid as uuid_lib
from common import Profile, LocalGame, Player, User, PlayerType

from typing import TYPE_CHECKING
import pickle

from common import GameStatus
from common.utils import hard_coded_profile

if TYPE_CHECKING:
    from client.data.data_controller import DataController

from common.interfaces.i_ihm_main_calls_data import I_IHMMainCallsData

DEFAULT_TILE_NUMBER = 165
DEFAULT_STATUS = GameStatus.AVAILABLE
DEFAULT_ROUND = 0


class IhmMainCallsData_Impl(I_IHMMainCallsData):
    def __init__(self, data_controller: DataController):
        self.data_controller = data_controller

    def get_public_games(self) -> List:
        return self.data_controller.public_games

    def get_connected_players(self) -> List[Player]:
        return self.data_controller.connected_players

    def get_local_game_id(self) -> uuid_lib.UUID:
        pass

    def get_profile(self) -> Profile:
        return hard_coded_profile

    def create_new_profile(self, user: User) -> None:
        """
        Doesnt 'create' anything.
        Saves the user object to a file.
        Does not check if the nickname is already taken.
        """
        profile_path = DataController.USER_PATH + user.nickname + ".profile"
        with open(profile_path, "wb") as profile_file:
            pickle.dump(user, profile_file)

    def create_game(
        self, name: str, tower_nb: int, player_choice: PlayerType
    ) -> LocalGame:
        profile = self.get_profile()
        player = profile
        red_player = player if player_choice is PlayerType.RED else None
        white_player = player if player_choice is PlayerType.WHITE else None
        return LocalGame(
            name,
            profile,
            DEFAULT_TILE_NUMBER,
            tower_nb,
            DEFAULT_STATUS,
            red_player,
            white_player,
            DEFAULT_ROUND,
        )

    def export_current_profile(self, destination_path: str) -> None:
        pass

    def import_profile(self, path: str) -> Profile:
        pass

    def edit_current_profile(self, profile: Profile) -> None:
        pass

    def disconnect_from_app(self) -> None:
        """
        Remove the current user from the attached data_controller
        Could be improved by saving the current myuser to a file, but this leads to mypy errors in the commented way.
        """
        # if backup_current_user_to_disk:
        #     self.create_new_profile()
        self.data_controller.myself = None

    def disconnect_server(self) -> None:
        """
        Remove datacontroller server-related data
        """
        self.data_controller.connected_players = []
        self.data_controller.public_games = []
        self.data_controller.local_game = None

    def connect_to_app(self, nickname: str, password: str) -> None:
        pass
